function create_shortcut() {
    echo "#!/bin/sh" > "/usr/bin/$1"
    echo "$2 \$@" >> "/usr/bin/$1"
    chmod +x "/usr/bin/$1"
}

echo "" >> /etc/pacman.conf
echo "[multilib]" >> /etc/pacman.conf
echo "Include = /etc/pacman.d/mirrorlist" >> /etc/pacman.conf

echo "" >> /etc/pacman.conf
echo "SigLevel = Never" >>  /etc/pacman.conf

# pacman-key --init
# pacman-key --populate archlinux
pacman-key --refresh-keys

pacman -Syyu --noconfirm
pacman -S --noconfirm wine wine-mono wine-gecko libunwind wget xorg-server-xvfb git python-requests python-pip sshpass rsync imagemagick python-pyqt6 qt5-tools appstream
pip install --break-system-packages pyproject-appimage

# Setup Windows Python inside Wine
export WINEDEBUG=-all
wget https://www.python.org/ftp/python/3.13.1/python-3.13.1-amd64.exe -O python-installer.exe
Xvfb :0 -screen 0 1024x768x16 &
export DISPLAY=:0.0
wine ./python-installer.exe /quiet InstallAllUsers=1 PrependPath=1 Include_test=0
create_shortcut "wine-pip" "wine python -m pip"
wine-pip install pyinstaller
create_shortcut "wine-pyinstaller" "wine pyinstaller"

# Install NSIS
wget "https://prdownloads.sourceforge.net/nsis/nsis-3.08-setup.exe?download" -O nsis-installer.exe
wine ./nsis-installer.exe /S "/D=C:\NSIS"
create_shortcut "makensis" "wine \"C:\\NSIS\\bin\\makensis.exe\""

git config --global safe.directory '*'

# Workaround because this file is not marked as executable now
chmod +x /usr/bin/pyuic6
