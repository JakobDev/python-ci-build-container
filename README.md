# python-ci-build-container

This is a little Docker Container that contains some Scripts to build Python Apps in Woodpecker.
This Container is only for my personal Usage. Things are free to change all the time, so please don't use it.
However, you are free to copy the scripts and modify they for your need.

## Scripts

### init-container
Setup the whole Container including Wine.

### set-file-env
Reads a text file and sets the content as environment variable

### full-python-build
Builds a Exe, A Windows Installer and a AppImage and places it in `Output`

### sourceforge-upload
Uploads the Directory to Sourceforge