FROM archlinux:latest

COPY init-container.sh /usr/bin/init-container
RUN chmod +x /usr/bin/init-container
# RUN /usr/bin/init-container

COPY full-python-build.py /usr/bin/full-python-build
RUN chmod +x /usr/bin/full-python-build

COPY sourceforge-upload.py /usr/bin/sourceforge-upload
RUN chmod +x /usr/bin/sourceforge-upload

COPY set-file-env.sh /usr/bin/set-file-env
RUN chmod +x /usr/bin/set-file-env

COPY write-changelog-readme.py /usr/bin/write-changelog-readme
RUN chmod +x /usr/bin/write-changelog-readme

ENV DISPLAY=:0.0
ENV WINEDEBUG=-all
