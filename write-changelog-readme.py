#!/usr/bin/env python
import requests
import argparse
import sys
import os


def main() -> None:
    parser = argparse.ArgumentParser()
    parser.add_argument("--name", required=True)
    parser.add_argument("--version", required=True)
    parser.add_argument("--output-file", required=True)
    args = parser.parse_args()

    r = requests.get(f"https://codeberg.org/api/v1/repos/JakobDev/{args.name}/releases/tags/{args.version}")

    if r.status_code != 200:
        print("Could not get changelog from Codeberg API", file=sys.stderr)
        print(str(r.json()), file=sys.stderr)
        sys.exit(1)

    output_file = os.path.abspath(args.output_file)

    try:
         os.makedirs(os.path.dirname(output_file))
    except FileExistsError:
         pass

    with open(output_file, "w", encoding="utf-8", newline="\n") as f:
        f.write(f"# Changelog for {args.version}\n\n")
        f.write(r.json()["body"])


if __name__ == "__main__":
    main()
