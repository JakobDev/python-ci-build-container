#!/usr/bin/env python
import subprocess
import requests
import argparse
import shutil
import sys
import os


def check_bin(name: str) -> None:
    if not shutil.which(name):
        print(f"{name} was not found. Make sure it's installed and in PATH", file=sys.stderr)
        sys.exit(1)


def get_env(name: str) -> str:
    if (env_var := os.getenv(name)) is not None:
         return env_var
    else:
        print(f"Enviroment Variable {name} is not set", file=sys.stderr)
        sys.exit(1)


def main() -> None:
    parser = argparse.ArgumentParser()
    parser.add_argument("--upload-dir", required=True)
    parser.add_argument("--project", required=True)
    parser.add_argument("--version", required=True)
    parser.add_argument("--app-name")
    parser.add_argument("--codeberg-release",  action="store_true")
    args = parser.parse_args()

    check_bin("sshpass")
    check_bin("rsync")

    password = get_env("SOURCEFORGE_PASSWORD")
    username = get_env("SOURCEFORGE_USERNAME")
    api_key = get_env("SOURCEFORGE_API_KEY")

    if args.codeberg_release:
        codeberg_api_token = get_env("CODEBERG_API_TOKEN")
        codeberg_tag = get_env("CI_COMMIT_TAG")
        codeberg_repo = get_env("CI_REPO")

    upload_list = []
    for i in os.listdir(args.upload_dir):
        upload_list.append(os.path.join(args.upload_dir, i))

    subprocess.run(["sshpass", "-p", password, "rsync", "-avP", "-e", "ssh -o StrictHostKeyChecking=no"] + upload_list + [f"{username}@frs.sourceforge.net:/home/frs/p/{args.project}/{args.version}"], check=True)

    if args.app_name is not None:
        requests.put(f"https://sourceforge.net/projects/{args.project}/files/{args.version}/{args.app_name}-{args.version}-Python.zip", {"default": "mac&default=android&default=bsd&default=solaris&default=others", "api_key": api_key},  headers={"Accept": "application/json"})
        requests.put(f"https://sourceforge.net/projects/{args.project}/files/{args.version}/{args.app_name}-{args.version}-WindowsInstaller.exe", {"default": "windows", "api_key": api_key},  headers={"Accept": "application/json"})
        requests.put(f"https://sourceforge.net/projects/{args.project}/files/{args.version}/{args.app_name}-{args.version}-Linux.AppImage", {"default": "linux", "api_key": api_key},  headers={"Accept": "application/json"})

    if args.codeberg_release:
        headers = {"accept": "application/json", "authorization": "token " + codeberg_api_token}
        release_id = requests.get(f"https://codeberg.org/api/v1/repos/{codeberg_repo}/releases/tags/{codeberg_tag}", headers=headers).json()["id"]

        for i in os.listdir(args.upload_dir):
            if i == "README.md" or i.endswith("-Python.zip"):
                continue

            r = requests.post(f"https://codeberg.org/api/v1/repos/{codeberg_repo}/releases/{release_id}/assets?name={i}", files={"external_url": (None, f"https://sourceforge.net/projects/{args.project}/files/{args.version}/{i}")}, headers=headers)


if __name__ == "__main__":
    main()
