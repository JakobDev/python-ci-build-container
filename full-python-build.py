#!/usr/bin/env python
import subprocess
import argparse
import tempfile
import shutil
import sys
import os


def create_directory(path: str) -> None:
    try:
        os.makedirs(path)
    except FileExistsError:
        pass


def compile_ui() -> None:
    if not os.path.exists("tools/CompileUI.py"):
        return

    subprocess.run([sys.executable, "tools/CompileUI.py"], check=True)


def build_translations() -> None:
    if not os.path.exists("tools/BuildTranslations.py"):
        return

    subprocess.run([sys.executable, "tools/BuildTranslations.py"], check=True)


def create_source_zip(name: str, version: str) -> None:
    subprocess.run(["git", "archive", "--format", "zip", "--output", f"Output/{name}-{version}-Python.zip", "HEAD"], check=True)


def windows_build(name: str, version: str, copy_exe: str | None) -> None:
    subprocess.run(["wine-pip", "install", "-r", "requirements.txt"])
    subprocess.run(["wine-pyinstaller", "--additional-hooks-dir=.", "--add-data", f"{name};{name}", "-i", "deploy/icon-windows.ico", "--windowed", "-y", f"{name}.py"], check=True)
    create_directory("WindowsBuild/Portable")
    shutil.copytree(f"dist/{name}", "WindowsBuild/Portable", dirs_exist_ok=True)

    if copy_exe is not None:
        if os.path.isfile(f"WindowsBuild/Portable/{copy_exe}"):
            os.remove(f"WindowsBuild/Portable/{copy_exe}")

        shutil.copyfile(f"WindowsBuild/Portable/{name}.exe", f"WindowsBuild/Portable/{copy_exe}")

    subprocess.run(["makensis", "deploy/Installer.nsi"], check=True)
    shutil.copyfile(f"deploy/{name}Installer.exe", f"Output/{name}-{version}-WindowsInstaller.exe")
    shutil.make_archive(f"Output/{name}-{version}-WindowsPortable", "zip", "WindowsBuild/Portable")


def appimage_build(name: str, version: str) -> None:
    subprocess.run(["pyproject-appimage", "--no-fuse", "--output", f"Output/{name}-{version}-Linux.AppImage"], check=True)


def main() -> None:
    parser = argparse.ArgumentParser()
    parser.add_argument("--name", required=True)
    parser.add_argument("--version", required=True)
    parser.add_argument("--copy-windows-exe")
    args = parser.parse_args()

    create_directory("Output")

    compile_ui()
    build_translations()

    create_source_zip(args.name, args.version)
    windows_build(args.name, args.version, args.copy_windows_exe)
    appimage_build(args.name, args.version)


if __name__ == "__main__":
    main()
